package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        try {
            Collections.sort(inputNumbers);
        } catch (Throwable e) {
            throw new CannotBuildPyramidException();
        }

        // use arithmetic series formula to find how many numbers would be in the last row if the list is convertable to pyramid;
        double numbersInRow = (Math.pow((1 + 8 * inputNumbers.size()), 0.5) - 1) / 2;
        if (numbersInRow%1!=0) throw new CannotBuildPyramidException();

        int height = (int)numbersInRow;
        int width = height * 2 - 1;
        int[][] pyramid = new int[height][width];

        int curRow = 0;
        int curNumbersInRow = 0;
        int curColumn = width / 2;

        for (int number : inputNumbers) {
            pyramid[curRow][curColumn] = number;
            if (++curNumbersInRow == curRow + 1) {
                curRow++;
                curNumbersInRow = 0;
                curColumn = width / 2 - curRow;
            } else curColumn += 2;
        }
        return pyramid;
    }
}