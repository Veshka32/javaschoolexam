package com.tsystems.javaschool.tasks.calculator;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;


public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if (statement == null || statement.length() == 0) return null;

        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);

        try {
            double result = parse(statement);
            return df.format(result);
        } catch (InvalidInputException e) {
            return null;
        } catch (NumberFormatException e){
            return null;
        }
    }

    private static double parse(String statement) throws InvalidInputException {

        StringBuilder buffer = new StringBuilder();
        LinkedList<Double> operands = new LinkedList<>();
        LinkedList<Character> operators = new LinkedList<>();
        int i=0;

        if (statement.charAt(i)=='-') {
            operands.add(-1.0);
            operators.add('*');
            i++;
        }

        for (; i < statement.length(); i++) {
            char c = statement.charAt(i);
            if (Character.isDigit(c) || c == '.') {
                buffer.append(c);
            } else if (c == '+' || c == '-' || c == '*' || c == '/') {
                if (buffer.length() > 0) {
                    operands.add(Double.parseDouble(buffer.toString()));
                    buffer.setLength(0);
                }
                operators.add(c);
                if (operands.size() != operators.size())
                    throw new InvalidInputException();

            } else if (c == '(') {
                if (buffer.length() > 0)
                    throw new InvalidInputException(); //left parentheses may come only after operator
                int leftParentheses = 1;
                int rightParentheses = 0;
                buffer.append(c);
                while (leftParentheses != rightParentheses && ++i < statement.length()) {
                    c = statement.charAt(i);
                    if (c == '(') leftParentheses++;
                    else if (c == ')') rightParentheses++;
                    buffer.append(c);
                }
                if (leftParentheses != rightParentheses) throw new InvalidInputException();
                operands.add(parse(buffer.substring(1, buffer.length() - 1)));
                buffer.setLength(0);
            } else
                throw new InvalidInputException();
        }

        if (buffer.length() > 0)
            operands.add(Double.parseDouble(buffer.toString()));

        if (operands.size() - operators.size() != 1)
            throw new InvalidInputException();

        return eval(operands, operators);
    }

    private static double eval(LinkedList<Double> operands, LinkedList<Character> operators) throws InvalidInputException{
        if (operands.size() == 1)
            return operands.get(0);
        double firstOperand = operands.get(0);
        Character operator = operators.get(0);
        switch (operator) {
            case '+':
                operands.removeFirst();
                operators.removeFirst();
                return firstOperand + eval(operands, operators);
            case '-':
                operands.set(1, operands.get(1) * -1);
                operands.removeFirst();
                operators.removeFirst();
                return firstOperand + eval(operands, operators);
            case '*':
                firstOperand *= operands.get(1);
                operands.set(1, firstOperand);
                operands.removeFirst();
                operators.removeFirst();
                return eval(operands, operators);
            case '/':
                if (operands.get(1) == 0) throw new InvalidInputException();
                firstOperand /= operands.get(1);
                operands.set(1, firstOperand);
                operands.removeFirst();
                operators.removeFirst();
                return eval(operands, operators);
        }
        throw new InvalidInputException();
    }
}